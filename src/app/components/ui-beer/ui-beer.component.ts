import { Component, Input, OnInit } from '@angular/core';
import { Beer } from '@app/models/app.model';

@Component({
  selector: 'ui-beer',
  templateUrl: './ui-beer.component.html',
  styleUrls: ['./ui-beer.component.scss'],
})
export class UiBeerComponent implements OnInit {
  @Input()
  public beer?: Beer;

  @Input()
  public thumbnail = true;

  constructor() {}

  ngOnInit(): void {}
}
