import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { icons, LucideAngularModule } from 'lucide-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiBeerComponent } from './components/ui-beer/ui-beer.component';
import { BeerDetailPageComponent } from './pages/beer-detail-page/beer-detail-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UiHeaderComponent } from './components/ui-header/ui-header.component';
import { UiFooterComponent } from './components/ui-footer/ui-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BeerDetailPageComponent,
    UiBeerComponent,
    UiHeaderComponent,
    UiFooterComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, LucideAngularModule.pick(icons)],
  providers: [],
  bootstrap: [AppComponent],
  exports: [],
})
export class AppModule {}
